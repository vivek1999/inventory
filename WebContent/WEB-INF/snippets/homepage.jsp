<%@ page import="testpack.Item,java.util.ArrayList" language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<table class="center" width="70%">

<%@taglib uri="http://vivekpatel" prefix="Vivek"%>
<Vivek:name name="${name}"/>

	<tr>
		<td align=left>Welcome: <h3><i>${name}</i></h3></td>
		<td align=right>
			<a href="EditAccount">Edit Account</a> 
			<a href="Logout">LogOut</a>
		</td>
	</tr>
	<tr>
		<td colspan=2 align=center>
			<h2>List of Items</h2>
			
			<table class="center">
				<tr><th>Actions</th><th>Item Name</th><th>Quantity</th></tr>
				<%--<%
					ArrayList<Item> allItems = (ArrayList<Item>)request.getAttribute("allItems");
					for(Item item : allItems) {
						--%>
						
			
			<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
			<c:forEach var="item" items="${allItems}">
							<tr>
								<td>
									<a href="EditItem?id=${item.id}">Edit</a> 
									<a href="DeleteItem?id=${item.id}">Delete</a>
									<a href="ViewItem?id=${item.id}">View</a>
								</td>
								<td>${item.name}</td>
								<td>${item.qty}</td>
							</tr>
			</c:forEach>		
			</table>
		</td>
	</tr>
</table>
<a href="AddItem">Add an item</a>