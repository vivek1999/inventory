package testpack;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/DeleteItem")
public class DeleteItem extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession sess=request.getSession();		
		if(sess.getAttribute("uid") != null) {
			int iid=Integer.parseInt(request.getParameter("id"));
			DB_Access db = new DB_Access();
			db.deleteItem(iid);
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/pages/deleteitem.jsp");
			rd.forward(request, response);	
			response.sendRedirect("DeleteItem");

		}
		else {
			response.sendRedirect("Login?msg=must login first");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
