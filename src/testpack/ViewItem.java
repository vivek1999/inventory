package testpack;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ViewItem
 */
@WebServlet("/ViewItem")
public class ViewItem extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
			}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession sess = request.getSession();
   		
   		if(sess.getAttribute("uid") == null) {
   			// user did not login ,send him back to login..
   		response.sendRedirect("Login?msg=you have to login first");
   		}
   		else {
   			// user successfully logged it, show the items
   			
   			DB_Access db = new DB_Access();
   			int uid = (Integer) sess.getAttribute("uid");
   			String uName = db.getUserName(uid);
   			ArrayList<Item> all = db.getAllUserItems(uid);
   			
   			response.setContentType("text/html");
   			PrintWriter out = response.getWriter();
   			response.setContentType("text/html");

			
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/pages/viewitem.jsp");
		rd.forward(request, response);		
	}

	}

}
