package testpack;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/EditAccount")
public class EditAccount extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer uid = (Integer) request.getSession().getAttribute("uid");
		if(uid == null) {
			// not logged in, send to Login with error message
			response.sendRedirect("Login?msg=have to login first...");
		}
		else {
			// show the home page
			DB_Access db = new DB_Access();
			String uname = db.getUserName(uid); request.setAttribute("name", uname);
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/pages/editaccount.jsp");
			rd.forward(request, response);			
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sess = request.getSession();
		int uid = (Integer) sess.getAttribute("uid");
		String username= request.getParameter("username");
		String name= request.getParameter("name");
		String loginPass1= request.getParameter("loginPass1");
		String loginPass2= request.getParameter("loginPass2");
		
		DB_Access db = new DB_Access();
		User u = new User(uid, name, username, loginPass1, loginPass2);
		db.updateAccount(u);
	
		response.sendRedirect("Login");
	}
}