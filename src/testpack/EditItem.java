package testpack;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class EditItem
 */
@WebServlet("/EditItem")
public class EditItem extends HttpServlet {
	private static final long serialVersionUID = 1L;

protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	request.getRequestDispatcher("/WEB-INF/pages/edititem.jsp").forward(request, response);
}
 
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	HttpSession sess = request.getSession();
	String name=request.getParameter("iname");
	String iqty=request.getParameter("iqty");
	DB_Access db = new DB_Access();
	int iid = Integer.parseInt(request.getParameter("id"));
	Integer uid = (Integer) request.getSession().getAttribute("uid");
	Item i = new Item();
	i.setId(iid);
	i.setUid(uid);
	i.setName(name);
	i.setQty(Integer.parseInt(iqty));
	db.updateItem(i);
	response.sendRedirect("Home");
	}

}
