package testpack;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class Name extends SimpleTagSupport {

	private String name;
	
	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();
		out.println("<h1>Hello "+name+" !!!</h1>");
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
}
