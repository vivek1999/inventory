package testpack;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/AddItem")
public class AddItem extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {			

		HttpSession sess=request.getSession();
		if(sess.getAttribute("uid") != null) {
			String iname= request.getParameter("name");
			String iqty= request.getParameter("qty");
			int uid= (Integer) sess.getAttribute("uid");
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/pages/additem.jsp");
			rd.forward(request, response);		

			DB_Access db=new DB_Access();
			int qty=0;
		
			try {
				qty=Integer.parseInt(iqty);
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			db.addItem(iname, iqty, uid);
			response.sendRedirect("AddItem");
		}
		else {
			response.sendRedirect("Login?msg=must login first");
		}

	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}
}